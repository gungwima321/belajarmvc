<?php 

class About extends Controller{
      public function index($nama='Gung',$profesi='pelajar',$umur='17'){
            $data['nama'] = $nama;
            $data['profesi'] = $profesi;
            $data['umur'] = $umur;
            $data['judul'] = 'About Me';
            $this->view('templates/header',$data);
            $this->view('about/index',$data);
            $this->view('templates/footer');
      }
      public function page(){
            $data['judul'] = 'Halaman Pages';
            $this->view('templates/header',$data);
            $this->view('about/page');
            $this->view('templates/footer');
      }
}
?>